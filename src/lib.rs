pub mod db;
mod oauth;
pub mod routes;
mod schema;

use async_trait::async_trait;
use diesel::Connection;
use reqwest::Client;
use rocket::{
    fairing::{self, Fairing, Info, Kind},
    Build, Rocket,
};
use serde::Deserialize;

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
#[derive(Clone)]
pub struct OAuthConfig {
    pub client_id: String,
    pub client_secret: String,
    pub gitlab_url: String,
    pub own_base_url: String,
    pub allowed_users: Option<Vec<String>>,
}

pub struct OAuthSupport {
    oauth_config: OAuthConfig,
}

impl OAuthSupport {
    pub fn fairing(oauth_config: OAuthConfig) -> OAuthSupport {
        OAuthSupport { oauth_config }
    }
}

#[derive(Deserialize)]
struct DbConfig {
    url: String,
}

#[derive(Deserialize)]
struct DbsConfig {
    tokens: DbConfig,
}

#[async_trait]
impl Fairing for OAuthSupport {
    fn info(&self) -> Info {
        Info {
            name: "OAuth Authentication support",
            kind: Kind::Ignite,
        }
    }

    async fn on_ignite(&self, rocket: Rocket<Build>) -> fairing::Result {
        let dbs_config: DbsConfig = rocket
            .figment()
            .extract_inner("databases")
            .expect("Reading db config");

        rocket::tokio::task::spawn_blocking(move || {
            let mut conn =
                diesel::SqliteConnection::establish(&dbs_config.tokens.url).expect("No database");
            crate::db::run_migrations(&mut conn).expect("Failed to run migrations");
        })
        .await
        .unwrap();

        Ok(rocket
            .attach(crate::db::Db::fairing())
            .register("/", crate::routes::catchers())
            .mount("/", crate::routes::routes())
            .manage(crate::oauth::OAuth2::new(&self.oauth_config).unwrap())
            .manage(self.oauth_config.clone())
            .manage(Client::new())
            .manage(routes::CsrfState::new_state()))
    }
}
