// @generated automatically by Diesel CLI.

diesel::table! {
    tokens (token) {
        token -> Text,
        username -> Text,
        timestamp -> BigInt,
    }
}

diesel::table! {
    users (username) {
        username -> Text,
        display_name -> Text,
    }
}

diesel::allow_tables_to_appear_in_same_query!(tokens, users,);
