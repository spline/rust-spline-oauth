use crate::schema::*;
use diesel::{prelude::*, Insertable};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use rocket_sync_db_pools::database;
use std::{error::Error, time::SystemTime};

#[database("tokens")]
pub struct Db(diesel::SqliteConnection);

pub(crate) fn now_in_unix() -> i64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
        .try_into()
        .expect("Value doesn't fit into i64 anymore. Time to upgrade!")
}

#[derive(Insertable)]
pub(crate) struct Token {
    pub token: String,
    pub username: String,
    pub timestamp: i64,
}

impl Db {
    pub(crate) async fn create_token(&self, token: Token) -> QueryResult<()> {
        self.run(move |c| diesel::insert_into(tokens::table).values(token).execute(c))
            .await?;
        Ok(())
    }

    pub(crate) async fn identify_user(&self, token: String) -> QueryResult<Option<String>> {
        self.run(move |c| {
            tokens::table
                .filter(tokens::token.eq(token))
                .select(tokens::username)
                .first(c)
                .optional()
        })
        .await
    }

    pub async fn delete_tokens(&self, username: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::delete(tokens::table.filter(tokens::username.eq(username))).execute(c)
        })
        .await?;
        Ok(())
    }

    pub(crate) async fn expire_tokens(&self) -> QueryResult<()> {
        self.run(move |c| {
            let now = now_in_unix();
            // This is a bit ugly, but I can't find an abs function or way to substract timestamp from now in diesel.
            // So the result will always be negative. If it is less than minus a day, the token has been created more than a day ago.
            diesel::delete(tokens::table.filter((tokens::timestamp - now).lt(-24 * 60 * 60)))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub(crate) async fn add_account_display_name(
        &self,
        username: String,
        display_name: String,
    ) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_or_ignore_into(users::table)
                .values((
                    users::username.eq(username.clone()),
                    users::display_name.eq(display_name.clone()),
                ))
                .execute(c)?;

            diesel::update(users::table)
                .filter(users::username.eq(username))
                .set(users::display_name.eq(display_name))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub(crate) async fn get_display_name(&self, username: String) -> QueryResult<Option<String>> {
        self.run(move |c| {
            users::table
                .filter(users::username.eq(username))
                .select(users::display_name)
                .first::<String>(c)
                .optional()
        })
        .await
    }
}

const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations/");

pub(crate) fn run_migrations(
    connection: &mut diesel::SqliteConnection,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    connection.run_pending_migrations(MIGRATIONS)?;
    Ok(())
}
