// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::OAuthConfig;
use oauth2::basic::BasicClient;
use oauth2::reqwest::async_http_client;
use oauth2::{
    AccessToken, AuthUrl, AuthorizationCode, ClientId, ClientSecret, CsrfToken, RedirectUrl, Scope,
    TokenResponse, TokenUrl,
};
use url::Url;

pub struct OAuth2 {
    client: BasicClient,
}

impl OAuth2 {
    pub fn new(config: &OAuthConfig) -> Result<OAuth2, url::ParseError> {
        Ok(OAuth2 {
            client: BasicClient::new(
                ClientId::new(config.client_id.to_string()),
                Some(ClientSecret::new(config.client_secret.to_string())),
                AuthUrl::new(format!("{}/oauth/authorize", config.gitlab_url))?,
                Some(TokenUrl::new(format!("{}/oauth/token", config.gitlab_url))?),
            )
            // Set the URL the user will be redirected to after the authorization process.
            .set_redirect_uri(RedirectUrl::new(format!(
                "{}/oauth/authorize",
                config.own_base_url
            ))?),
        })
    }

    pub fn get_authorization_url(&self) -> (Url, CsrfToken) {
        // Generate the full authorization URL.
        self.client
            .authorize_url(CsrfToken::new_random)
            // Set the desired scopes.
            .add_scope(Scope::new("read_user".to_string()))
            .url()
    }

    pub async fn oauth_authorize(&self, code: String) -> anyhow::Result<AccessToken> {
        // Once the user has been redirected to the redirect URL, you'll have access to the
        // authorization code. For security reasons, your code should verify that the `state`
        // parameter returned by the server matches `csrf_state`.

        // Now you can trade it for an access token.
        let token_result = self
            .client
            .exchange_code(AuthorizationCode::new(code))
            // Set the PKCE code verifier.
            .request_async(async_http_client)
            .await?;

        // Unwrapping token_result will either produce a Token or a RequestTokenError.
        Ok(token_result.access_token().clone())
    }
}
