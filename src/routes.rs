use crate::{
    db::{self, now_in_unix, Db},
    oauth::OAuth2,
    OAuthConfig,
};
use log::error;
use reqwest::Client;
use rocket::{
    catch, catchers, get,
    http::{Cookie, CookieJar, SameSite, Status},
    request::{self, FromRequest, Outcome, Request},
    response::{status::Custom, Redirect},
    routes,
    tokio::sync::RwLock,
    uri, Catcher, Route, State,
};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fmt::Debug, sync::Arc};
use uuid::Uuid;

#[derive(Clone)]
struct CsrfContext {
    target_url: String,
}

pub type HttpResult<T> = Result<T, Custom<()>>;
pub struct CsrfState(Arc<RwLock<HashMap<String, CsrfContext>>>);

impl CsrfState {
    pub fn new_state() -> CsrfState {
        CsrfState(Arc::new(RwLock::new(HashMap::<String, CsrfContext>::new())))
    }
}

pub fn handle_database_error<T: Debug>(error: T) -> Custom<()> {
    error!("A database query failed: {:?}", error);
    Custom(Status::InternalServerError, ())
}

#[derive(Serialize)]
pub struct OauthUrl {
    url: String,
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
struct GitlabUser {
    username: String,
    name: Option<String>,
}

#[get("/oauth/authorize?<code>&<state>")]
async fn authorize(
    oauth: &State<OAuth2>,
    http_client: &State<Client>,
    oauth_config: &State<OAuthConfig>,
    csrf_tokens: &State<CsrfState>,
    cookies: &CookieJar<'_>,
    db: Db,
    code: String,
    state: String,
) -> HttpResult<Redirect> {
    let access_token = oauth.oauth_authorize(code).await.unwrap();

    if !csrf_tokens.0.read().await.contains_key(&state) {
        return Ok(Redirect::to(uri!("/")));
    }

    let gitlab_user = http_client
        .get(format!("{}/api/v4/user", oauth_config.gitlab_url))
        .header(
            "Authorization",
            &format!("Bearer {}", access_token.secret()),
        )
        .send()
        .await
        .expect("Fetching user info")
        .json::<GitlabUser>()
        .await
        .expect("Parsing user info");

    if let Some(allowed) = &oauth_config.allowed_users {
        if !allowed.contains(&gitlab_user.username) {
            return Err(Custom(Status::Forbidden, ()));
        }
    }

    let token = Uuid::new_v4().to_string();

    db.create_token(db::Token {
        token: token.clone(),
        username: gitlab_user.username.clone(),
        timestamp: now_in_unix(),
    })
    .await
    .map_err(handle_database_error)?;

    if let Some(name) = gitlab_user.name {
        db.add_account_display_name(gitlab_user.username.clone(), name)
            .await
            .map_err(handle_database_error)?;
    }

    let mut cookie = Cookie::new("session-token", token);
    cookie.set_same_site(Some(SameSite::Lax));
    cookie.set_secure(true);
    cookie.set_http_only(true);
    cookie.set_expires(time::OffsetDateTime::now_utc() + time::Duration::days(1));
    cookies.add(cookie);

    if let Some(context) = csrf_tokens.0.read().await.get(&state) {
        let target_url = context.target_url.clone();
        return Ok(Redirect::to(target_url));
    }

    let tokens = csrf_tokens.0.read().await;
    match tokens.get(&state) {
        Some(context) => Ok(Redirect::to(context.target_url.clone())),
        None => {
            error!("Login request had no context");
            Err(Custom(Status::InternalServerError, ()))
        }
    }
}

#[get("/oauth/login?<target_url>")]
pub async fn login(
    oauth: &State<OAuth2>,
    csrf_state: &State<CsrfState>,
    target_url: String,
) -> Redirect {
    let (url, csrf_token) = oauth.get_authorization_url();
    csrf_state
        .0
        .write()
        .await
        .insert(csrf_token.secret().to_string(), CsrfContext { target_url });

    Redirect::to(url.to_string())
}

pub(crate) fn routes() -> Vec<Route> {
    routes![login, authorize]
}

pub(crate) fn catchers() -> Vec<Catcher> {
    catchers![catch_unauthorized]
}

#[derive(Clone)]
pub struct AuthenticatedUser {
    pub username: String,
    pub token: String,
}

impl AuthenticatedUser {
    pub async fn get_display_name(&self, db: &db::Db) -> diesel::QueryResult<Option<String>> {
        db.get_display_name(self.username.clone()).await
    }
}

#[derive(Debug)]
pub enum AuthError {
    Unauthorized,
    Server,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthenticatedUser {
    type Error = AuthError;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        let header = req.headers().get("session-token").next();
        let cookie = req.cookies().get("session-token").map(Cookie::value);
        match header.or(cookie) {
            Some(token) => {
                let db = Db::from_request(req)
                    .await
                    .expect("The database fairing should have been already registered");

                db.expire_tokens()
                    .await
                    .expect("Failed to remove expired tokens");

                match db.identify_user(token.to_string()).await {
                    Ok(username) => match username {
                        Some(username) => Outcome::Success(AuthenticatedUser {
                            username,
                            token: token.to_string(),
                        }),
                        None => Outcome::Error((Status::Unauthorized, AuthError::Unauthorized)),
                    },
                    Err(error) => {
                        error!("Database error {:#?}", error);
                        Outcome::Error((Status::InternalServerError, AuthError::Server))
                    }
                }
            }
            None => Outcome::Error((Status::Unauthorized, AuthError::Unauthorized)),
        }
    }
}

#[catch(401)]
async fn catch_unauthorized<'r>(request: &Request<'r>) -> Redirect {
    Redirect::to(uri!(login(request.uri().to_string())))
}
